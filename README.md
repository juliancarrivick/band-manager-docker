# Band Manager Docker

This project puts together the 
[Backend](https://gitlab.com/juliancarrivick/band-manager-backend)
and the 
[Frontend](https://gitlab.com/juliancarrivick/band-manager-frontend)
of the band manager running them in docker containers to ease of deployment.

- To start the containers run `docker-compose up`
- Then, add an admin user: `docker exec <backend-container> bin/cake createUser admin admin`
- Then, you can login using credentials `admin/admin` at `localhost:5100`

